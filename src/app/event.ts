export class Event {
    public id: number;
    public name: string;
    public date: string;
    public startTime: string;
    public endTime: string;
    public description: string;
    public street1: string;
    public street2: string;
    public city: string;
    public state: string;
    public zipcode: string;
    public category: EventCategory;
    public longitude: number;
    public latitude: number;

    constructor(id: number, name:string, date: string, startTime: string, endTime: string, description:string, street1: string, street2:string, city: string, state: string, zipcode: string, longitude: number, latitude:number, category:EventCategory) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.description = description;
        this.street1 = street1;
        this.street2 = street2;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.longitude = longitude;
        this.latitude = latitude;
        this.category = category;
    }

    public validate(): boolean {
        /** TODO */
        return true;
    }
}

export class EventCategory {
    constructor(id: number, name: string, description: string) { }

    public validate(): boolean {
        /** TODO */
        return true;
    }
}

export class VolEvent {
     id: number;
     name: string;
  //  public date: string;
     startDate: string;
     endDate: string;
     desc: string;
     street1: string;
     street2: string;
     city: string;
     state: string;
     zipCode: string;
     categoryId: string;
     longitude: number;
     latitude: number;
}
