export class Donation {
    constructor(public name: String, public email: String,
                public category: string, public amount: number) { }
}

export class DonationCategory {
    constructor(id: number, name: string, description: string) { }
}
