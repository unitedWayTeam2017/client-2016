import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import { DonationCategory } from './give/give';
import { User } from './user';
import { AppComponent } from './app.component';
import { Event, EventVolunteer } from './volunteer/event';
import { Coordinates } from './volunteer-item/coordinates';

@Injectable()
export class AppService {
  private rootUrl = 'https://uwde-rest.herokuapp.com/';
  static isAdmin = false;
  static isLoggedIn = false;

  constructor(private _http: Http) { //this.getCoordinates().subscribe(data=> console.log('Coordinate: ' + JSON.stringify(data)));
  }

  public isUserAdmin() {
    return AppService.isAdmin;
  }

  public setAdminUser() {
    AppService.isAdmin = true;
  }

  public isLogIn() {
    return AppService.isLoggedIn;
  }

  public setLogIn() {
    AppService.isLoggedIn = true;
    AppComponent.setLogin(true);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  saveDonation(donation): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(donation);
    console.log(JSON.stringify(options));
    return this._http.put(this.rootUrl + 'donation', body, options)
      .map((res: Response) => res.json());
  }

  register(eventVolunteer): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(eventVolunteer);
    console.log('options:' + JSON.stringify(options));
    return this._http.put(this.rootUrl + 'volunteer/insert', body, options)
      .map((res: Response) => res.json());
  }

  updateEvent(event): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(event);
    console.log(JSON.stringify(options));
    return this._http.put(this.rootUrl + 'events/update', body, options)
      .map((res: Response) => res.json());
  }

  createEvent(event): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(event);
    console.log(JSON.stringify(options));
    return this._http.put(this.rootUrl + 'events/insert', body, options)
      .map((res: Response) => res.json());
  }

  getDonationCategories(): Observable<DonationCategory[]> {
    return this._http.get(this.rootUrl + 'donationCategory')
      .map((res: Response) => <DonationCategory[]>res.json())
      .do(data => console.log('Donation Category: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getVols(id: number): Observable<EventVolunteer[]> {
    return this._http.get(this.rootUrl + 'volunteer/' +id)
    .map((res: Response)=> <EventVolunteer[]>res.json())
//    .do(data => console.log('Events: ' + JSON.stringify(data)))
    .catch(this.handleError);
  }

  getEvents(): Observable<Event[]> {
    return this._http.get(this.rootUrl + 'events')
    .map(this.extractData)
//    .do(data => console.log('Events: ' + JSON.stringify(data)))
    .catch(this.handleError);
  }

  getEvent(id: number): Observable<Event[]> {
    console.log('service');
    return this._http.get(this.rootUrl + 'events/' + id)
      .map(this.extractData)
      .do(data => console.log('Event: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    var data = res.json() || [];
    data.forEach((d) => {
      d.startDate = new Date(d.startDate);
      d.endDate = new Date(d.endDate);
    });
    return <Event[]>data;
  }

  getCoordinates(addr: string): Observable<Coordinates> {
    return this._http.get('http://maps.google.com/maps/api/geocode/json?address='+addr)
      .map(this.extractCoordinates)
      .do(data => console.log('Coordinate: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractCoordinates(res: Response) {
    var data = res.json();
//    console.log(data.results[0].geometry.location);
    return <Coordinates>data.results[0].geometry.location;
  }

  emailTaken(desired: string): Observable<User> {
    return this._http.get(this.rootUrl + 'login/emailTaken?desiredName=' + desired)
 //     .do(data => console.log(data))
      .catch(this.handleError);
  }

  addNewUser(user: User): Observable<User> {
    console.log("In service " + user);
    return this._http.post(this.rootUrl + 'login/addNewUser', user)
 //     .do(data => console.log(data))
      .catch(this.handleError);
  }

  login(user: User): Observable<string> {
    console.log("In service " + user);
    return this._http.post(this.rootUrl + 'login/login', user)
      .map((res: Response) => res.json())
      //.do(data => console.log(data))
      .catch(this.handleError);
  }
}
