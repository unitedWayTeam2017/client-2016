import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { User } from '../user';
import { Router } from '@angular/router'

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
  providers: [AppService]
})
export class SignUpComponent implements OnInit {

  public user: User;
  public retype: string;

  constructor(private appService: AppService, private router: Router) { 
  	this.user = new User();  
  }

  ngOnInit() {
  	//this.appService.emailTaken('mtm@gmail.com')
    //  .subscribe(data => console.log(data));
  }

  onSubmit(){
  	this.appService.addNewUser(this.user)
      .subscribe(data => {
      	//console.log(data);
      	this.router.navigateByUrl('/');
      });
  }

}
