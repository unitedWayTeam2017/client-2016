import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { VolunteerService } from '../volunteer.service';
import { Event } from '../volunteer/event';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-volunteer-list',
  templateUrl: './volunteer-list.component.html',
  styleUrls: ['./volunteer-list.component.css'],
  providers: [ AppService, VolunteerService ]
})
export class VolunteerListComponent implements OnInit {
  public events: Event[];
  public event = new Event('',null,null,'','','','','','','','');
  public from: Date;
  public to: Date;
  public days = 60;
  public vols: any[] = [];
  public createMode = false;
  public createText = 'Create Mode';
  public dt: Date;
  public stime;
  public etime;
  public isAdmin;

  constructor(private appService: AppService,private volServ: VolunteerService) { }

  ngOnInit() {
  //  this.volServ.getEvents()
  //    .then(events => this.events = events);
    this.from = new Date();
    this.to = new Date(this.from.getTime() + this.days*24*60*60*1000);
      this.appService.getEvents()
        .subscribe(events => this.events=events);
      this.isAdmin = this.appService.isUserAdmin();
  }

  toggleCreateMode(event){
    if(this.createMode){
      this.createMode=false;
      this.createText='Create Mode';
    }
    else{
      this.createMode=true;
      this.createText='Exit Create Mode';
    }
  }

  createNewEvent(){
  var dat = new Date(this.dt);
  const[shh,smm] = this.stime.split(':');
  const[ehh,emm] = this.etime.split(':');
    this.event.startDate = new Date(dat.getFullYear(),dat.getMonth(),dat.getDate()+1,shh,smm);
    this.event.endDate = new Date(dat.getFullYear(),dat.getMonth(),dat.getDate()+1,ehh,emm);
    //console.log(new Date(dat.getFullYear(),dat.getMonth(),dat.getDate()+1,shh,smm));
    var addr = this.event.street1 + ',' + this.event.street2 + ',' + this.event.city + ',' + this.event.state + ',' + this.event.zipCode;
    addr = addr.replace(/\s+/g, '+');

    this.appService.getCoordinates(addr)
      .subscribe(data=> {
        console.log('addr: ' + JSON.stringify(data));
        this.event.latitude=data.lat;
        this.event.longitude=data.lng;
        console.log("Created event: " + JSON.stringify(this.event));
        this.appService.createEvent(this.event)
          .subscribe(data => {
            console.log('Event Updated: '+ JSON.stringify(data));
            //event = new Event('',null,null,'','','','','','','','');
            this.createMode = false;
          });

        //this.createMode = false;
      });
  //  console.log("Creaetd event: " + JSON.stringify(this.event[0]));
//alert(addr);

  }

}
