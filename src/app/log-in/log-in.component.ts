import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { User } from '../user';
import { Response, } from '@angular/http';
import { Router } from '@angular/router'

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css'],
  providers: [AppService]
})
export class LogInComponent implements OnInit {

  public user: User;
  private showError: boolean = false;
  public login = false;
  public adminEmail = 'admin@uwde.org';

  constructor(private appService: AppService, private router: Router) {
  	this.user = new User();
  }

  ngOnInit() {
  }

  onSubmit(){
  	var valid;
  	this.appService.login(this.user)
  	.subscribe(data => {
      console.log('login');
      console.log(this.user);
      console.log(data);
      if (data){
        //console.log("login!");
        if (this.user.email == this.adminEmail) {
          this.appService.setAdminUser();
        }
        this.appService.setLogIn();
        this.router.navigateByUrl('volunteer');
      }
      else{
      	this.showError = true;
      }
  	});
  }

}
