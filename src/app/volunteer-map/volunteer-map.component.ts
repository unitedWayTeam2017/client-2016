import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { VolunteerService } from '../volunteer.service';
import { Event } from '../volunteer/event';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-volunteer-map',
  templateUrl: './volunteer-map.component.html',
  styleUrls: ['./volunteer-map.component.css'],
  providers: [AppService, VolunteerService]
})
export class VolunteerMapComponent implements OnInit {
  public events: Event[];
  public vols: any[] = [];
  constructor(private appService: AppService, private volServ: VolunteerService) { }
  lng: number = -75.5498539;
  lat: number = 39.7466083;
  zoom: number = 10;
  public from: Date;
  public to: Date;
  public days = 60;

  ngOnInit() {
  this.from = new Date();
  this.to = new Date(this.from.getTime() + this.days*24*60*60*1000);
//    this.volServ.getEvents()
//      .then(events => this.events = events);
    this.appService.getEvents()
      .subscribe(events => this.events=events);
  }

}
