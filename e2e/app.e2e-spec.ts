import { UnitedWayDelawarePage } from './app.po';

describe('united-way-delaware App', function() {
  let page: UnitedWayDelawarePage;

  beforeEach(() => {
    page = new UnitedWayDelawarePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
